from wrapper import TestTemplate, Proto

import pytest
import ast


class TestJsonHTTP(TestTemplate):
    def test_ok(self):
        self.execute({
            'index': 'category_0',
            'match': 'iPhone'
        }, Proto.JSON)

    def test_incorrect_query(self):
        with pytest.raises(Exception):
            self.execute({
                'index': 'category_0',
            }, Proto.JSON)

        with pytest.raises(Exception):
            self.execute({
                'match': 'iPhone'
            }, Proto.JSON)


class TestClassBasic(TestTemplate):
    def test_ping(self):
        code = self.ping()
        assert code == 200

    def test_syntax_error(self):
        with pytest.raises(Exception):
            self.execute('select *;')

    def test_no_such_index(self):
        with pytest.raises(Exception):
            self.execute('select * from test;')

    def test_dummy_index(self):
        result = self.execute({
            'index': 'category_0',
            'match': 'hi'
        }, Proto.JSON)
        result = ast.literal_eval(result)

        assert len(result) == 0

        result = self.execute({
            'index': 'category_0',
            'match': 'Защитное стекло'
        }, Proto.JSON)
        result = ast.literal_eval(result)

        assert len(result) > 0
        assert result[0] == [400804, 4]
