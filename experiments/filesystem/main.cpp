#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace fs = std::filesystem;

const std::string default_index_storage_name = "storage.bin";

using Index = std::map <std::string, std::vector <int> >;

Index GetIndexFromIstream(std::istream& in) {
    int words_cnt;
    in >> words_cnt;
    std::map <std::string, std::vector <int> > index;
    for (int i = 0; i < words_cnt; ++i) {
        int hits_cnt;
        std::string word;
        in >> word >> hits_cnt;
        int hit;
        auto& hits = index[word];
        hits.reserve(hits_cnt);
        for (int j = 0; j < hits_cnt; ++j) {
            in >> hit;
            hits.push_back(hit);
        }
    }
    return index;
}

inline size_t GetStringBinaryLen(const std::string& s) {
    return s.length() * sizeof(char) + sizeof(int);
}


template <typename T>
inline void ReadValue(std::istream& in, T& value) {
    in.read(reinterpret_cast<char *>(&value), sizeof(T));
}

template <typename T>
inline void WriteValue(std::ostream& out, T& value) {
    out.write(reinterpret_cast<char *>(&value), sizeof(T));
}


void WriteBinaries(const Index& index) {
    int kPeriod = 2;
    int current = 0;
    int prefound_cnt = index.size() / kPeriod;
    std::vector <std::pair <std::string, size_t> > prefound_words;
    prefound_words.reserve(prefound_cnt);
    size_t prefix_len = prefound_cnt * sizeof(size_t) + 2 * sizeof(int);
    size_t total_offset = 0;

    for (auto it = index.begin(); it != index.end(); ++it) {
        if (++current == kPeriod) {
            current = 0;
            prefix_len += GetStringBinaryLen(it->first);
            prefound_words.push_back({it->first, total_offset});
        }
        total_offset += GetStringBinaryLen(it->first) + sizeof(int);
        if (!it->second.empty()) {
            total_offset += it->second.size() * sizeof(decltype(it->second.front()));
        }
    }

    std::cout << "Prefix length: " << prefix_len << std::endl;
    std::fstream fs(default_index_storage_name, std::ios::out | std::ios::binary | std::ios::trunc);
    {
        int total_words = index.size();
        fs.write(reinterpret_cast<char *>(&total_words), sizeof(int));
        fs.write(reinterpret_cast<char *>(&prefound_cnt), sizeof(int));
    }

    for (auto& [word, word_offset]: prefound_words) {
        int word_len = word.length();
        fs.write(reinterpret_cast<char *>(&word_len), sizeof(int));
        fs.write(word.data(), sizeof(char) * word_len);
        size_t current_offset = word_offset + prefix_len;
        fs.write(reinterpret_cast<char *>(&current_offset), sizeof(size_t));
    }

    for (auto& [word, hits]: index) {
        int word_len = word.size();
        fs.write(reinterpret_cast<char *>(&word_len), sizeof(int));
        fs.write(word.data(), sizeof(char) * word_len);
        int hits_cnt = hits.size();
        fs.write(reinterpret_cast<char *>(&hits_cnt), sizeof(int));
        for (auto& hit: hits) {
            int current_hit = hit;
            fs.write(reinterpret_cast<char *>(&current_hit), sizeof(int));
        }
    }
}


void ReadBinaries() {
    std::fstream fs(default_index_storage_name, std::ios::in | std::ios::binary);
    size_t total_offset = 0;

    int prefound_cnt = 0, total_words = 0;
    fs.read(reinterpret_cast<char*>(&total_words), sizeof(int));
    fs.read(reinterpret_cast<char*>(&prefound_cnt), sizeof(int));
    total_offset += 2 * sizeof(prefound_cnt);
    std::cout << "Total words amount: " << total_words << '\n';
    std::cout << "Prefound words amount: " << prefound_cnt << '\n';

    for (int i = 0; i < prefound_cnt; ++i) {
        int word_len;
        ReadValue(fs, word_len);
        std::string word(word_len, 0);
        fs.read(word.data(), word_len);

        size_t word_offset;
        ReadValue(fs, word_offset);
        total_offset += sizeof(int) + word.length() * sizeof(char) + sizeof(size_t);
        std::cout << word << ": " << word_offset << '\n';
    }
    std::cout << "Prefix length: " << total_offset << std::endl;
    std::cout << '\n';

    for (int i = 0; i < total_words; ++i) {
        int word_len;
        ReadValue(fs, word_len);
        std::string word(word_len, 0);
        fs.read(word.data(), word_len);

        int hits_cnt;
        ReadValue(fs, hits_cnt);
        std::cout << word << " -> offset: " << total_offset << ", hits amount: " << hits_cnt << "\nhits: ";
        total_offset += sizeof(int) * (2 + hits_cnt) + sizeof(char) * word_len;
        for (int j = 0; j < hits_cnt; ++j) {
            int hit;
            ReadValue(fs, hit);
            std::cout << hit;
            if (j + 1 < hits_cnt) {
                std::cout << ", ";
            }
        }
        std::cout << '\n';
    }
}

void TestFromStdin() {
    auto index = GetIndexFromIstream(std::cin);
    std::cout << "Got index from stream" << std::endl;
    WriteBinaries(index);
    std::cout << "Wrote index to file" << std::endl;
    ReadBinaries();
    std::cout << "Read index from file" << std::endl;
}

void SimpleTest() {
    std::istringstream input("6\n"
                             "a 3\n"
                             "1 2 3\n"
                             "b 3\n"
                             "2 3 4\n"
                             "c 2\n"
                             "4 1\n"
                             "ed 4\n"
                             "1 2 3 5\n"
                             "aav 5\n"
                             "2 5 3 4 1\n"
                             "hova 2\n"
                             "2 5\n");
    auto index = GetIndexFromIstream(input);
    std::cout << "Got index from stream" << std::endl;
    WriteBinaries(index);
    std::cout << "Wrote index to file" << std::endl;
    ReadBinaries();
    std::cout << "Read index from file" << std::endl;
}

int main() {
    SimpleTest();
}

/*
3
a 3
1 2 3
b 3
2 3 4
c 2
4 1
 */