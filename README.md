# Bigdata infrastructure course
* [Таблица результатов](https://docs.google.com/spreadsheets/d/1ohDdHefRp1fIyfoiDmCorqXxAziqSqDzVUDuSunbqdU/edit?usp=sharing)

| task-name   | deadline    | penalty | score |
|-------------|-------------|---------|-------|
| http        | 01.03 23:59 |    0%   |100|
| ram-segment | 08.03 23:59 |    0%   |200|
| index-me    | 15.03 23:59 |    0%   |500|
| bm25        | 26.03 23:59 |    50%  |200|
| plain       | 31.03 23:59 |    50%  |800|
| compression | 31.03 23:59 |    50%  |200|
