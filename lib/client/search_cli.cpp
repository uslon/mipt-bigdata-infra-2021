#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/URI.h>

#include <iostream>
#include <memory>

#include "cmdparser.h"
#include "variadic_table.h"

void runSqlCommand(Poco::Net::HTTPClientSession& session,
	CommandLineParser& parser) {
	Poco::Net::HTTPRequest request("GET", "/sql_command");
	std::string query = parser.GetQuery();
	request.setContentLength(query.length());
	std::ostream& request_stream = session.sendRequest(request);
	request_stream << query;
	request_stream.flush();

	Poco::Net::HTTPResponse response;
	auto& response_stream = session.receiveResponse(response);

	auto result = Poco::JSON::Parser().parse(response_stream);
	Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();
	std::string error = object->get("error").toString();

	if (!error.empty())
	{
		std::cerr << error << std::endl;
	} else
	{
		VariadicTable<std::string, std::string> vt(
			{ "document_id", "weight" }, 10);


		// Your code goes here...
		std::istringstream data_stream(object->get("data").toString());
		std::string doc_id, doc_size;
		data_stream >> doc_id >> doc_size;

		vt.addRow(doc_id, doc_size);
		vt.print(std::cout);
	}
}

int getJSONContentLength(std::shared_ptr<Poco::JSON::Object> json) {
	std::ostringstream oss;
	json->stringify(oss);
	return oss.str().length();
}

void runJSONCommand(Poco::Net::HTTPClientSession& session,
	CommandLineParser& parser) {
	Poco::Net::HTTPRequest request("GET", "/json_command");
	std::string query = parser.GetQuery();

	auto json = std::make_shared<Poco::JSON::Object>();
	json->set("index", "basic_index");
	json->set("match", query);

	request.setContentLength(getJSONContentLength(json));
	std::ostream& request_stream = session.sendRequest(request);
	json->stringify(request_stream);
	request_stream.flush();

	Poco::Net::HTTPResponse response;
	auto& response_stream = session.receiveResponse(response);

	auto result = Poco::JSON::Parser().parse(response_stream);
	Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();
	std::string error = object->get("error").toString();

	if (!error.empty())
	{
		std::cerr << error << std::endl;
	} else
	{
		VariadicTable<std::string, std::string> vt(
			{ "document_id", "weight" }, 10);


		// Your code goes here...
		std::istringstream data_stream(object->get("data").toString());
		std::string doc_id, doc_size;
		data_stream >> doc_id >> doc_size;

		vt.addRow(doc_id, doc_size);
		vt.print(std::cout);
	}
}


int main(int argc, char * argv[])
{
	Poco::Net::HTTPClientSession session("127.0.0.1", 5849);

	{
		Poco::Net::HTTPRequest request("GET", "/ping");
		session.sendRequest(request);
		Poco::Net::HTTPResponse response;
		session.receiveResponse(response);

		if (response.getStatus() != 200)
		{
			std::cerr << "Can't connect to server" << std::endl;
			return -1;
		}
	}

	bool proto = (argc > 1 && !strcmp(argv[1], "--proto"));

	CommandLineParser parser(std::cin, std::cout);
	while (parser.Parse())
	{
		if (parser.IsCompleted())
		{
			if (proto)
			{
				runJSONCommand(session, parser);
			} else
			{
				runSqlCommand(session, parser);
			}
		}
	}


	return 0;
}