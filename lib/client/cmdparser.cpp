#include "cmdparser.h"

#include <algorithm>

CommandLineParser::CommandLineParser(std::istream & in, std::ostream & out)
	: in_(in)
	, out_(out)
{}

bool CommandLineParser::Parse()
{
	// Your code goes here...

	if (completed_line_)
	{
		out_ << "mipt-search";
	}
	out_ << "> ";

	query_.clear();
	std::string local_buffer;
	if (completed_line_ && !buffer_.empty()) {
		std::string new_line;
		if (!getline(in_, new_line) && buffer_.find(';') == std::string::npos) {
			return false;
		}

		local_buffer = std::move(buffer_);
		std::copy(new_line.begin(), new_line.end(), std::back_inserter(local_buffer));
		buffer_.clear();
	} else if (!getline(in_, local_buffer)) {
		return false;
	}

	completed_line_ = false;

	auto it = find(local_buffer.begin(), local_buffer.end(), ';');
	if (it != local_buffer.end()) {
		std::copy(local_buffer.begin(), next(it), std::back_inserter(buffer_));
		query_ = std::move(buffer_);
		buffer_.clear();
		std::copy(next(it), local_buffer.end(), std::back_inserter(buffer_));
		return completed_line_ = true;
	}

	std::copy(local_buffer.begin(), local_buffer.end(), std::back_inserter(buffer_));
	buffer_.push_back(' ');
	return true;
}

bool CommandLineParser::IsCompleted() const
{
	return !query_.empty();
}

std::string CommandLineParser::GetQuery() const
{
	return query_;
}
