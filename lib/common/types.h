#pragma once

#include <cassert>
#include <cinttypes>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

#define c_size(container) static_cast<int>((container).size())

const uint32_t ALL_FIELDS = 3;
const double MIN_IDF = 1e-5;

struct IndexStats
{
	uint32_t m_uDocumentCount = 0;
	uint32_t m_uDocumentTotalLen = 0;
};

struct WordMetadata
{
	double IDF = MIN_IDF;
};

struct IndexMetadata {
	std::unordered_map<std::wstring, WordMetadata> WordsMetadata;
	std::unordered_map<uint32_t, uint32_t> DocSizes;
	double AvgDocSize;
};


struct Match
{
	uint32_t m_iDocumentId;
};

struct Hit
{
	uint32_t m_iDocumentId;
	uint8_t m_bFieldId;
	uint8_t m_bHitPos;
};

enum class RankerType
{
	DummyRanker,
	BM25
};

enum class IndexType
{
	Template,
	Dummy,
	Plain,
	RT
};

inline IndexType GetIndexTypeFromName(const std::string & name)
{
	if (name == "template")
	{
		return IndexType::Template;
	} else if (name == "dummy")
	{
		return IndexType::Dummy;
	} else if (name == "plain")
	{
		return IndexType::Plain;
	} else if (name == "rt")
	{
		return IndexType::RT;
	}

	throw std::runtime_error("Unknown index name");
}

inline std::string GetIndexNameFromType(IndexType type)
{
	switch (type)
	{
		case IndexType::Template: return "template";
		case IndexType::Dummy: return "dummy";
		case IndexType::Plain: return "plain";
		case IndexType::RT: return "rt";
	}

	assert(0);
}

struct Context;

struct Query
{
	std::string m_sIndexName;
	int m_iLimit = -1;

	std::vector<std::pair<std::wstring, uint32_t>> m_dMatchFieldMask;

	RankerType m_iRankerType;

	std::shared_ptr<Context> m_pContext;
};

inline bool QueryHasMatch(const Query & query)
{
	return !query.m_dMatchFieldMask.empty();
}