#include "handlers.h"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include <sstream>

#include "lib/common/types.h"
#include "lib/server/morphology/stemming.h"
#include "lib/server/net/context.h"
#include "lib/server/sql/ast.h"
#include "lib/server/sql/driver.hh"
#include "lib/std/string.h"

namespace search
{
void Ping::handleRequest(Poco::Net::HTTPServerRequest & request,
	Poco::Net::HTTPServerResponse & response)
{
	logInfo("Ping got");

	response.send().flush();
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
}

HandleSqlCommand::HandleSqlCommand(IndexStorage & storage)
	: storage_(storage)
{}

int getJSONContentLength(std::shared_ptr<Poco::JSON::Object> json) {
	std::ostringstream oss;
	json->stringify(oss);
	return oss.str().length();
}

void HandleSqlCommand::handleRequest(Poco::Net::HTTPServerRequest & request,
	Poco::Net::HTTPServerResponse & response)
{

	char command[4096];

	request.stream().read(command, 4096);
	logInfo("Sql command: " << command);

	auto response_json = std::make_shared<Poco::JSON::Object>(true);
	response_json->set("error", "");
	response_json->set("data", "324 720\n");

	Driver parse_driver;
	auto query = parse_driver.Parse(command);

	response.setContentLength(getJSONContentLength(response_json));
	std::ostream& response_stream = response.send();
	response_json->stringify(response_stream);
	response_stream.flush();

	// Response error
	// logInfo("Query type: " << (query->type == sql::AstType::string ? "sql::String" : "Other"));
	if (query == nullptr)
	{
		// Your code goes here...
		response.send().flush();
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
		return;
	}

	/** Prepare query **/

	/** Execute query **/
	// Your code goes here...

	/** Response query **/
	// Your code goes here...
	response.send().flush();
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
}

HandlerFactory::HandlerFactory(IndexStorage & storage)
	: storage_(storage)
{}

Poco::Net::HTTPRequestHandler * HandlerFactory::createRequestHandler(
	const Poco::Net::HTTPServerRequest & request)
{
	if (request.getMethod() != Poco::Net::HTTPRequest::HTTP_GET)
		return nullptr;

	logInfo("Get query with endpoint: " << request.getURI());

	if (request.getURI() == "/ping")
	{
		return new Ping();
	}

	if (request.getURI() == "/sql_command")
	{
		return new HandleSqlCommand(storage_);
	}

	if (request.getURI() == "/json_command")
	{
		return new HandleJSONCommand(storage_);
	}

	return nullptr;
}

HandleJSONCommand::HandleJSONCommand(IndexStorage & storage): storage_(storage)
{
}

void HandleJSONCommand::handleRequest(Poco::Net::HTTPServerRequest & request, Poco::Net::HTTPServerResponse & response)
{

	auto parser_result = Poco::JSON::Parser().parse(request.stream());
	Poco::JSON::Object::Ptr json = parser_result.extract<Poco::JSON::Object::Ptr>();


	std::string error;
	if (!json->has("index")) {
		error = "No index option was provided";
	} else if (!json->has("match")) {
		error = "No query string was provided";
	}

	auto response_json = std::make_shared<Poco::JSON::Object>(true);
	response_json->set("error", error);

	logInfo("error: " + error);
	if (!error.empty()) {
		response.setContentLength(getJSONContentLength(response_json));
		response_json->stringify(response.send());
		response.send().flush();
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_NOT_ACCEPTABLE);
		return;
	}
	logInfo("match: " + json->get("match").toString())


	std::vector <String> words = ParseWords(ToWstring(json->get("match").toString()));
	std::vector <std::pair<std::wstring, uint32_t > > match_field_mask;
	match_field_mask.reserve(words.size());
	for (String& word: words) {
		match_field_mask.emplace_back(std::move(word), ALL_FIELDS);
	}

	// prepare query
	auto context = std::make_shared<Context>();
	Poco::JSON::Array array;
	auto pArray = std::make_shared<Poco::JSON::Array>(array);
	RowWriter rw(pArray);
	context->pRowWriter = std::make_shared<RowWriter>(rw);
	Query q = {
		json->get("index"),
		-1,
		std::move(match_field_mask),
		RankerType::BM25,
		context
	};

	// execute query
	storage_.GetIndex(json->get("index"))->ExecuteSelect(q);

	// response query
	std::stringstream ss;
	pArray->stringify(ss);
	logInfo("array: " << ss.str());
	response_json->set("data", ss.str());
	response.setContentLength(getJSONContentLength(response_json));
	response_json->stringify(response.send());
	response.send().flush();
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
}
} // namespace search