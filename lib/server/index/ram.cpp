#include "ram.h"

#include <lib/common/types.h>
#include <sys/stat.h>
#include <lib/server/morphology/morph.h>

#include <codecvt>
#include <regex>
#include <set>
#include <sstream>
#include <unordered_map>
#include <unordered_set>

#include "lib/server/engine/ranking.h"
#include "lib/server/net/context.h"
#include "lib/std/csv_parser.h"
#include "lib/std/string.h"

namespace search
{
RamSegment::RamSegment(const std::shared_ptr<IndexConfig> & pIndexConfig)
{
	// Init morphology
	m_pFilter = CreateFilter(pIndexConfig);

	uifstream iIn(pIndexConfig->sSource);

	// Building ram index
	if (iIn.is_open())
	{
		logInfo("Build RAM index from file " << pIndexConfig->sSource);

		std::unordered_map<String, int> word_document_cnt;

		String line;
		getline(iIn, line); // skip dataset description
		while (getline(iIn, line)) {
			auto parsed_line = CsvParser::ParseLine(line);
			++m_sStats.m_uDocumentCount;
			std::unordered_set<String> words;
			auto doc_id = std::stoi(parsed_line[0]);
			AddField(parsed_line[1], words, doc_id, 0);
			AddField(parsed_line[2], words, doc_id, 1);

			m_dMetadata.DocSizes[doc_id] = ParseWords(parsed_line[1]).size() + ParseWords(parsed_line[2]).size();
			for (const auto& word: words) {
				++word_document_cnt[word];
			}
		}

		for (auto& [word, metadata]: m_dMetadata.WordsMetadata) {
			metadata.IDF = std::max(
				MIN_IDF,
				log(m_sStats.m_uDocumentCount - word_document_cnt[word] + 0.5) /
					(word_document_cnt[word] + 0.5) + 1);
		}

		if (m_sStats.m_uDocumentCount > 0) {
			m_dMetadata.AvgDocSize = static_cast<double>(m_sStats.m_uDocumentTotalLen) /
															m_sStats.m_uDocumentCount;
		} else {
			m_dMetadata.AvgDocSize = 1;
		}

		logInfo("Index ready");
	} else
	{
		logFatal("Can't open source " << pIndexConfig->sSource);
	}
}

void RamSegment::ExecuteSelect(const Query & tQuery)
{
	// Your code goes here...
	// help: create ranker, proceed hits
	// You are able to compute hits and then proceed them
	// Sort result
	// Use tQuery.m_pContext to save your results
	auto ranker = CreateRanker(tQuery.m_iRankerType);
	ranker->Init(m_dMetadata);
	auto query_hits = GetQueryHits(tQuery);

	std::vector <std::pair <double, uint32_t> > ranks;
	ranks.reserve(query_hits.size());
	for (const auto& [doc_id, hits]: query_hits) {
		for (const auto& [hit, word]: hits) {
			ranker->ProceedHit(hit, word);
		}
		ranks.emplace_back(ranker->Complete(), doc_id);
	}

	sort(ranks.begin(), ranks.end(), [](const std::pair <double, uint32_t>& a, const std::pair <double, uint32_t>& b) {
		return (a.first > b.first) || (a.first == b.first && a.second < b.second);
	});
	for (auto [rank, doc_id]: ranks) {
		tQuery.m_pContext->pRowWriter->Write({doc_id, rank});
	}
}

std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> RamSegment::GetQueryHits(
	const Query & tQuery)
{
	static auto fInMask = [](uint32_t iMask, uint8_t iPos) {
		return iMask & (1u << iPos);
	};

	std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> dWordMatch;

	for (const auto& [word, mask]: tQuery.m_dMatchFieldMask) {
		const auto& hits = m_dDataIndex[word];
		for (const auto& hit: hits) {
			if (fInMask(mask, hit.m_bFieldId)) {
				dWordMatch[hit.m_iDocumentId].push_back({hit, word});
			}
		}
	}
	return dWordMatch;
}

void RamSegment::AddField(const String & sField, std::unordered_set<String> & words_set, uint32_t iDocumentId, uint8_t iFieldId)
{
	auto words = ParseWords(sField);
	for (size_t i = 0; i < words.size(); ++i) {
		m_pFilter->Consume(words[i]);
		auto filtered = m_pFilter->Produce();
		if (filtered) {
			++m_sStats.m_uDocumentTotalLen;
			words_set.insert(filtered.value());
			m_dDataIndex[filtered.value()].push_back({iDocumentId, iFieldId, (uint8_t)i});
		}
	}
}

void RamSegment::Flush(const std::string & sPath)
{
	// Your code goes here...
}

} // namespace search