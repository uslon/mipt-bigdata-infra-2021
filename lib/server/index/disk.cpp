#include "disk.h"

#include <queue>

#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "lib/server/engine/ranking.h"
#include "lib/server/net/context.h"

using namespace search;

inline size_t GetStringBinaryLen(const String& s) {
	return s.length() * sizeof(wchar_t) + sizeof(int);
}

template <typename T>
inline void ReadValue(int fd, T& value) {
	read(fd, &value, sizeof(T));
}

inline void ReadString(int fd, String& s) {
	int s_len;
	ReadValue(fd, s_len);
	s.resize(s_len);
	read(fd, s.data(), s_len * sizeof(wchar_t));
}


namespace search
{
void DiskSegment::ExecuteSelect(const Query & tQuery)
{
	auto ranker = CreateRanker(tQuery.m_iRankerType);
	ranker->Init(index_metadata_);
	auto query_hits = GetQueryHits(tQuery);

	std::vector <std::pair <double, uint32_t> > ranks;
	ranks.reserve(query_hits.size());
	for (const auto& [doc_id, hits]: query_hits) {
		for (const auto& [hit, word]: hits) {
			ranker->ProceedHit(hit, word);
		}
		ranks.emplace_back(ranker->Complete(), doc_id);
	}

	sort(ranks.begin(), ranks.end(), [](const std::pair <double, uint32_t>& a, const std::pair <double, uint32_t>& b) {
	  return (a.first > b.first) || (a.first == b.first && a.second < b.second);
	});
	for (auto [rank, doc_id]: ranks) {
		tQuery.m_pContext->pRowWriter->Write({doc_id, rank});
	}
}

DiskSegment::DiskSegment(const std::shared_ptr<IndexConfig> & p_index_config)
{
	fd_ = open(p_index_config->sIndexDataPath.c_str(), O_RDONLY);
	if (fd_ == -1) {
		logError("Couldn't open index binary file.");
		return;
	}

	ReadValue(fd_, total_words_);
	ReadValue(fd_, header_words_cnt_);
	ReadValue(fd_, index_stats_);
	index_metadata_.AvgDocSize = index_stats_.m_uDocumentTotalLen /
		static_cast<double>(index_stats_.m_uDocumentCount);

	index_metadata_.DocSizes.reserve(index_stats_.m_uDocumentCount);
	for (size_t i = 0; i < index_stats_.m_uDocumentCount; ++i) {
		int doc_id, doc_size;
		ReadValue(fd_, doc_id);
		ReadValue(fd_, doc_size);
		index_metadata_.DocSizes[doc_id] = doc_size;
	}

	header_offset_ = total_data_len_ = 2 * sizeof(int) + sizeof(index_stats_)
		+ index_stats_.m_uDocumentCount * 2 * sizeof(int);
	for (int i = 0; i < header_words_cnt_; ++i) {
		String str;
		size_t offset;
		ReadString(fd_, str);
		ReadValue(fd_, offset);
		total_data_len_ += sizeof(int) + sizeof(wchar_t) * str.length() + sizeof(size_t);
	}

	for (int i = 0; i < total_words_; ++i) {
		String word;
		int hit_cnt;
		double idf;
		ReadString(fd_, word);
		ReadValue(fd_, hit_cnt);
		lseek(fd_, hit_cnt * sizeof(Hit), SEEK_CUR);
		ReadValue(fd_, idf);
		index_metadata_.WordsMetadata[word] = { idf };
		total_data_len_ += GetStringBinaryLen(word) + sizeof(int)
			+ hit_cnt * sizeof(Hit) + sizeof(double);
	}
	lseek(fd_, 0, SEEK_SET);
	data_ = static_cast<char *>(mmap(
		nullptr,
		total_data_len_,
		PROT_READ,
		MAP_PRIVATE,
		fd_,
		0));
}

DiskSegment::~DiskSegment()
{
	munmap(data_, total_data_len_);
}

std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> DiskSegment::GetQueryHits(
	const Query & tQuery)
{
	static auto fInMask = [](uint32_t iMask, uint8_t iPos) {
	  return iMask & (1u << iPos);
	};

	std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> word_match;

	for (const auto& [word, mask]: tQuery.m_dMatchFieldMask) {
		auto hits = GetWordHits(FindRightBlock(word), word);
		for (const auto& hit: hits) {
			if (fInMask(mask, hit.m_bFieldId)) {
				word_match[hit.m_iDocumentId].push_back({hit, word});
			}
		}
	}
	return word_match;
}

int DiskSegment::FindRightBlock(const String & word)
{
	lseek(fd_, header_offset_, SEEK_SET);
	for (int i = 0; i < header_words_cnt_; ++i) {
		String header_word;
		size_t offset;
		ReadString(fd_, header_word);
		ReadValue(fd_, offset);
		if (word < header_word) {
			return i - 1;
		}
	}
	return offsets_.size() - 1;
}

std::vector<Hit> DiskSegment::GetWordHits(int block_id, const String & word)
{
	lseek(fd_, offsets_[block_id], SEEK_SET);
	std::vector<Hit> hits;
	size_t offset = offsets_[block_id];

	while (offset < total_data_len_ &&
		(block_id < offsets_.size() - 1 && offset < offsets_[block_id + 1])) {
		String current_word;
		ReadString(fd_, current_word);
		int hits_cnt;
		ReadValue(fd_, hits_cnt);
		if (current_word > word) {
			return hits;
		} else if (current_word == word) {
			hits.resize(hits_cnt);
			for (auto& hit: hits) {
				ReadValue(fd_, hit);
			}
			return hits;
		}
	}

}
} // namespace search