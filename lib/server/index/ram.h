#pragma once

#include <unordered_set>

#include "index.h"
#include "lib/common/config.h"
#include "lib/server/morphology/morph.h"
#include "lib/std/string.h"

namespace search
{
class RamSegment : public Index
{
public:
	using IndexData = std::unordered_map<String, std::vector<Hit>>;

	RamSegment() = default;
	explicit RamSegment(const std::shared_ptr<IndexConfig> & pIndexConfig);

	void ExecuteSelect(const Query & tQuery) override;

	void Flush(const std::string & sPath);

	inline void SetData(const IndexData & data) { m_dDataIndex = data; }
	inline IndexData GetData() const { return m_dDataIndex; }
	inline IndexMetadata GetMetadata() const { return m_dMetadata; }
	inline IndexStats GetStats() const { return m_sStats; }

	inline IndexData ExtractData() { return std::move(m_dDataIndex); }
	inline IndexMetadata ExtractMetadata() { return std::move(m_dMetadata); }

private:
	std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> GetQueryHits(
		const Query & tQuery);

	void AddField(const String & sField, std::unordered_set<String> & words_set, uint32_t iDocumentId, uint8_t iFieldId);

private:
	// Your code goes here...

	int m_iUsedRam = 0;

	IndexStats m_sStats;
	IndexMetadata m_dMetadata;
	IndexData m_dDataIndex;
};

} // namespace search