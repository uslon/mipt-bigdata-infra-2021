#include <string>
#include <unordered_map>
#include <vector>

#include "index.h"
#include "lib/common/config.h"
#include "lib/server/morphology/morph.h"
#include "lib/std/string.h"
#include "lib/common/types.h"

namespace search
{
class DiskSegment : public Index
{
public:
	using IndexData = std::unordered_map<String, std::vector<Hit>>;

	DiskSegment(const std::shared_ptr<IndexConfig>& p_index_config);
	void ExecuteSelect(const Query & tQuery) override;

	~DiskSegment();

private:
	std::unordered_map<uint32_t, std::vector<std::pair<Hit, String>>> GetQueryHits(
		const Query & tQuery);

	int FindRightBlock(const String& word);
	std::vector <Hit> GetWordHits(int block_id, const String& word);
	// Recommendation: write PlainPostingView

	std::vector<size_t> offsets_;
	IndexStats index_stats_;
	IndexMetadata index_metadata_;
	size_t total_data_len_;
	size_t header_offset_;
	int header_words_cnt_;
	int total_words_;
	int fd_;
	char * data_;

private:
	std::unordered_map<std::string, std::vector<Hit>> m_dDataIndex;
};

} // namespace search