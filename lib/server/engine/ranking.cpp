#include "ranking.h"

#include <stdexcept>

void DummyRanker::Init(const IndexMetadata & metadata)
{
	hit_count_ = 0;
	UNUSED(metadata);
};

void DummyRanker::ProceedHit(Hit hit, const search::String & word)
{
	++hit_count_;
	UNUSED(hit);
	UNUSED(word);
};

double DummyRanker::Complete()
{
	double res = hit_count_;
	hit_count_ = 0;
	return res;
}

void BM25Ranker::Init(const IndexMetadata & metadata)
{
	pMetadata = std::make_shared<IndexMetadata>(metadata);
}

void BM25Ranker::ProceedHit(Hit hit, const search::String & word)
{
	++m_hWordCount[word];
	doc_id = hit.m_iDocumentId;
}

double BM25Ranker::Complete()
{
	double sum = 0;
	double part = m_dK * (1 - m_dB + m_dB * pMetadata->DocSizes.at(doc_id) / pMetadata->AvgDocSize);
	for (const auto& [word, frequency]: m_hWordCount) {
		sum += pMetadata->WordsMetadata.at(word).IDF * frequency * (m_dK + 1)
			/ (frequency + part);
	}
	m_hWordCount.clear();
	return sum;
}

std::shared_ptr<IRanker> CreateRanker(RankerType rankerType)
{
	switch (rankerType)
	{
		case RankerType::DummyRanker:
		{
			return std::make_shared<DummyRanker>();
		}
		case RankerType::BM25:
		{
			return std::make_shared<BM25Ranker>();
		}
		default: throw std::runtime_error("Unimplemented ranker");
	}
}