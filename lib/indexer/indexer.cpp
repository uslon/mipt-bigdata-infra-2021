#include <filesystem>
#include <iostream>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "lib/common/config.h"
#include "lib/server/index/ram.h"
#include "lib/std/csv_parser.h"
#include "lib/std/logger.h"

using namespace search;

namespace fs = std::filesystem;

const std::string default_index_storage_name = "storage.bin";

inline size_t GetStringBinaryLen(const String& s) {
	return s.length() * sizeof(wchar_t) + sizeof(int);
}

template <typename T>
inline void ReadValue(int fd, T& value) {
	read(fd, &value, sizeof(T));
}

inline void ReadString(int fd, String& s) {
	int s_len;
	ReadValue(fd, s_len);
	s.resize(s_len);
	read(fd, s.data(), s_len * sizeof(wchar_t));
}

template <typename T>
inline void WriteValue(int fd, const T& value) {
	write(fd, &value, sizeof(T));
}

inline void WriteString(int fd, const String& s) {
	WriteValue(fd, static_cast<int>(s.length()));
	write(fd, s.data(), s.length() * sizeof(wchar_t));
}

/*
 * Pack index in the following way:
 *
 * {
 * --------------------- prefix -----------------------
 * total_words_cnt, header_words_cnt
 *
 * <index_stats>
 *
 * <metaData:
 * [doc_id, doc_size]
 * ...
 * [doc_id, doc_size]>
 *
 * <header:
 * [word, offset]
 * ...
 * [word, offset]>
 * --------------------- prefix -----------------------
 * <index:
 * [word, hits, idf]
 * ...
 * [word, hits, idf]>
 * }
 *
 * Although idf is a part of metaData it's stored with
 * hits in order to reduce the memory consumption
 */

void PackIndex(
	RamSegment::IndexData & indexData,
	IndexMetadata & indexMetadata,
	IndexStats & indexStats,
	const std::shared_ptr<IndexConfig> & pIndexConfig,
	const fs::path& sDataDir)
{
	size_t kN = 1024;
	size_t words_in_header = (static_cast<int>(indexData.size()) - 1) / kN + 1;
	size_t total_offset = 0;
	size_t prefix_len = 2 * sizeof(int) + sizeof(indexStats) +
		indexMetadata.DocSizes.size() * sizeof(int) * 2 + words_in_header * sizeof(size_t);
	std::vector <std::pair <String, size_t> > header_words;
	header_words.reserve(words_in_header);

	// calculate offsets for header
	size_t current_word = kN - 1;
	for (const auto& [word, hits]: indexData) {
		if (++current_word == kN) {
			current_word = 0;
			prefix_len += GetStringBinaryLen(word);
			header_words.emplace_back(word, total_offset);
		}
		// add to offset word + number of hits + idf
		total_offset += GetStringBinaryLen(word) + sizeof(int) + sizeof(double);
		if (!hits.empty()) {
			total_offset += hits.size() * sizeof(hits.front());
		}
	}

	int fd = open((sDataDir / default_index_storage_name).c_str(), O_WRONLY | O_CREAT);
	auto mmap_res = mmap(
		nullptr,
		total_offset + prefix_len,
		PROT_WRITE,
		MAP_PRIVATE,
		fd,
		0);
	if (mmap_res == MAP_FAILED) {
		logError("Couldn't create mapping.");
		return;
	}

	WriteValue(fd, static_cast<int>(indexData.size()));
	WriteValue(fd, static_cast<int>(words_in_header));
	WriteValue(fd, indexStats);

	// write metadata
	for (const auto& [doc_id, doc_size]: indexMetadata.DocSizes) {
		WriteValue(fd, doc_id);
		WriteValue(fd, doc_size);
	}

	// write header
	for (const auto& [word, word_offset]: header_words) {
		WriteString(fd, word);
		WriteValue(fd, word_offset + prefix_len);
	}

	// write index
	for (const auto& [word, hits]: indexData) {
		WriteString(fd, word);
		WriteValue(fd, static_cast<int>(hits.size()));
		for (const auto& hit: hits) {
			WriteValue(fd, hit);
		}
		WriteValue(fd, indexMetadata.WordsMetadata[word]);
	}

	munmap(mmap_res, total_offset + prefix_len);
}

void BuildIndexFromData(RamSegment::IndexData& indexData,
	IndexMetadata& indexMetadata,
	IndexStats& indexStats,
	const std::shared_ptr<IndexConfig> & pIndexConfig)
{
	RamSegment ramSegment(pIndexConfig);
	indexData = ramSegment.ExtractData();
	indexMetadata = ramSegment.ExtractMetadata();
	indexStats = ramSegment.GetStats();
}

void BuildIndex(const fs::path & sDataDir,
	const std::shared_ptr<IndexConfig> & pIndexConfig)
{
	RamSegment::IndexData indexData;
	IndexMetadata indexMetadata;
	IndexStats indexStats;
	BuildIndexFromData(indexData, indexMetadata, indexStats, pIndexConfig);


}

void PrintHelp()
{
	std::cout << "usage: indexer [config path]\n";
}

int main(int argc, char ** argv)
{
	if (argc != 2)
	{
		PrintHelp();
		return 1;
	}

	std::shared_ptr<SearchConfig> pConfig = nullptr;
	try
	{
		pConfig = LoadConfig(argv[1]);
	} catch (std::runtime_error & error)
	{
		logFatal("Config file '" << argv[1] << "': " << error.what());
	}

	int iSuccess = 0, iFailed = 0;
	for (auto & [sIndexName, pIndexConfig] : pConfig->index)
	{
		if (pIndexConfig->iIndexType != IndexType::Plain)
		{
			logWarning("Skipping non-plain index '" << sIndexName << "'...");
			continue;
		}

		try
		{
			BuildIndex(pConfig->indexDirectory, pIndexConfig);
			++iSuccess;
		} catch (std::runtime_error & error)
		{
			++iFailed;
			logWarning("Can't build plain index: " << error.what());
		}
	}

	return ((iSuccess > 0) && (iFailed == 0));
}