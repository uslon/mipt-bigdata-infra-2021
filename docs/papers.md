# Papers
* [MyStem stemming, Ilya Segalovich](https://cache-mskm907.cdn.yandex.net/download.yandex.ru/company/iseg-las-vegas.pdf)
* [Correlation and Prediction of Evaluation Metrics in Information Retrieval](https://arxiv.org/pdf/1802.00323.pdf)
* [pFound ranking metric by Yandex](http://romip.ru/romip2009/15_yandex.pdf)
* [ATC by hh.ru](http://romip.ru/romip2009/05_hh.pdf)
* [Ranking metrics](https://habr.com/ru/company/econtenta/blog/303458/)
* [Roaring bitmaps](https://arxiv.org/pdf/1402.6407.pdf) 
